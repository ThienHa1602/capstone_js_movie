import { userServ } from "../../services/userServices";
import { USER_LOGIN } from "../constant/userConstant";

export const setUserActionThunk = (fromData, onSuccess) => {
  return (dispatch) => {
    userServ
      .postLogin(fromData)
      .then((res) => {
        console.log(res);
        dispatch({
          type: USER_LOGIN,
          payload: res.data.content,
        });
        onSuccess(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
