import { BAT_LOADING, TAT_LOADING } from "../constant/spinnerConstant";

export const batLoadingAction = (payload) => ({
  type: BAT_LOADING,
});
export const tatLoadingAction = (payload) => ({
  type: TAT_LOADING,
});
