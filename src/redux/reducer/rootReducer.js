import { combineReducers } from "redux";
import spinnerReducer from "./spinnerReducer";
import userReducer from "./userReducer";

export const rootReducer = combineReducers({ spinnerReducer, userReducer });
