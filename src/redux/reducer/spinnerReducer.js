import { BAT_LOADING, TAT_LOADING } from "../constant/spinnerConstant";

const initialState = {
  isLoading: false,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case BAT_LOADING:
      return { ...state, isLoading: true };
    case TAT_LOADING:
      return { ...state, isLoading: false };
    default:
      return state;
  }
};
