import React from "react";
import { Desktop, Mobile } from "../../Layout/Responsive";
import HeaderDestop from "./HeaderDestop";
import HeaderMobile from "./HeaderMobile";

export default function Header() {
  return (
    <div>
      <Desktop>
        <HeaderDestop />
      </Desktop>
      <Mobile>
        <HeaderMobile />
      </Mobile>
    </div>
  );
}
