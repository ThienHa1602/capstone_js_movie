import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localUserServ } from "../../services/localServices";

export default function HeaderMobile() {
  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  let handleLogout = () => {
    localUserServ.remove();
    window.location.href = "/";
  };
  let renderUserNav = () => {
    let btnCss =
      "bg-orange-500 hover:bg-orange-400 text-white font-bold py-2 px-4 border-b-4 border-orange-700 hover:border-orange-500 rounded";
    if (userInfo) {
      return (
        <>
          <span>{userInfo.hoTen}</span>
          <button onClick={handleLogout} className={btnCss}>
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to="/login">
            <button className={btnCss}>Đăng nhập</button>
          </NavLink>
          <button className={btnCss}>Đăng ký</button>
        </>
      );
    }
  };
  return (
    <div>
      <div className="flex justify-between h-20 items-center shadow fixed top-0 left-0 right-0 ml-10 mr-10 z-50  bg-white">
        <NavLink to="/">
          <div className="text-orange-500 font-medium animate-bounce inline-block text-xl">
            <i class="fa fa-star"></i>
            <span>GalaxyFlix</span>
          </div>
        </NavLink>
        <div className="space-x-10 ">{renderUserNav()}</div>
      </div>
    </div>
  );
}
