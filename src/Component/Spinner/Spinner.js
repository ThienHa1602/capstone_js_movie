import React from "react";
import { useSelector } from "react-redux";
import { RingLoader } from "react-spinners";

export default function Spinner() {
  let { isLoading } = useSelector((state) => {
    return state.spinnerReducer;
  });
  return isLoading ? (
    <div
      style={{
        backgroundImage:
          "url(https://t3.ftcdn.net/jpg/05/37/17/24/360_F_537172411_C4GmfD6cpU7mrra2CYzyYUL34w3Bk1mN.jpg)",
      }}
      className="h-screen w-screen fixed top-0 left-0 z-50 flex justify-center items-center"
    >
      <RingLoader color="white" size={200} />
    </div>
  ) : (
    <></>
  );
}
