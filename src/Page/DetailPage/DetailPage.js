import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { movieServ } from "../../services/movieServices";
import { Progress } from "antd";
import TabDetail from "./TabDetail";
import { useDispatch } from "react-redux";
import {
  batLoadingAction,
  tatLoadingAction,
} from "../../redux/action/spinnerAction";

export default function DetailPage() {
  let params = useParams();
  let dispatch = useDispatch();
  const [movie, setMovie] = useState({});
  useEffect(() => {
    dispatch(batLoadingAction());
    movieServ
      .getDetailMovie(params.id)
      .then((res) => {
        dispatch(tatLoadingAction());
        setMovie(res.data.content);
      })
      .catch((err) => {
        dispatch(tatLoadingAction());
        console.log(err);
      });
  }, []);
  return (
    <div>
      <div
        style={{ padding: "0 20px 0 0" }}
        className="container-tab mb-20 flex justify-between space-x-10 items-center bg-white"
      >
        <img src={movie.hinhAnh} className="w-60" />
        <div className="space-y-10 ">
          <h2 className="text-3xl font-bold text-cyan-900 hover:tracking-wider duration-150">
            {movie.tenPhim}
          </h2>
          <p className="text-cyan-600 font-medium">{movie.moTa}</p>
        </div>
        <Progress
          type="circle"
          percent={movie.danhGia * 10}
          status="exception"
          format={(percent) => {
            return percent / 10 + "điểm";
          }}
          size={200}
        />
      </div>
      <div>
        <TabDetail id={params.id} />
      </div>
    </div>
  );
}
