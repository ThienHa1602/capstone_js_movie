import { Tabs } from "antd";
import React, { useEffect, useState } from "react";
import { movieServ } from "../../services/movieServices";
import moment from "moment";

export default function TabDetail({ id }) {
  const [movie, setMovie] = useState([]);
  useEffect(() => {
    movieServ
      .getMovieById(id)
      .then((res) => {
        console.log(res.data.content.heThongRapChieu);
        setMovie(res.data.content.heThongRapChieu);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderHeThongRapChieu = () => {
    return movie.map((heThongRap, index) => {
      return {
        key: index,
        label: <img src={heThongRap.logo} className="w-16" alt="" />,
        children: (
          <div>
            {heThongRap.cumRapChieu.map((item) => {
              return (
                <div className="m mt-10">
                  <h1 className="text-2xl mb-5 hover:text-orange-500 cursor-pointer">
                    {item.tenCumRap}
                  </h1>
                  <div className="grid grid-cols-5 gap-10">
                    {item.lichChieuPhim.map((item) => {
                      return (
                        <div className="gioChieuPhim">
                          <p className="text-black text-lg font-medium">
                            {moment(item.ngayChieuGioChieu).format(
                              "DD-MM-YYYY"
                            )}
                          </p>
                          <p className="text-orange-500 font-medium ">
                            {moment(item.ngayChieuGioChieu).format("LT")}
                          </p>
                        </div>
                      );
                    })}
                  </div>
                </div>
              );
            })}
          </div>
        ),
      };
    });
  };
  return (
    <div style={{ backgroundColor: "white" }} className="container-tab mb-20">
      <Tabs
        style={{ height: 600 }}
        tabPosition="left"
        defaultActiveKey="1"
        items={renderHeThongRapChieu()}
      />
    </div>
  );
}
