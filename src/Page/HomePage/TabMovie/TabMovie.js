import React, { useEffect, useState } from "react";
import { movieServ } from "../../../services/movieServices";
import { Tabs } from "antd";
import ChildrenTabMovie from "./ChildrenTabMovie";

export default function TabMovie() {
  const [dataMovie, setDataMovie] = useState([]);
  useEffect(() => {
    movieServ
      .getMovieByTheater()
      .then((res) => {
        console.log(res.data.content);
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderHeThongRap = () => {
    return dataMovie.map((heThongRap, index) => {
      return {
        key: index,
        label: <img src={heThongRap.logo} className="w-16" alt="" />,
        children: (
          <Tabs
            style={{ height: 600 }}
            tabPosition="left"
            items={heThongRap.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.diaChi,
                label: (
                  <p className=" text-orange-500">
                    {cumRap.tenCumRap.length > 25
                      ? cumRap.tenCumRap.slice(0, 25) + "..."
                      : cumRap.tenCumRap}
                  </p>
                ),
                children: <ChildrenTabMovie listMovie={cumRap.danhSachPhim} />,
              };
            })}
          />
        ),
      };
    });
  };
  return (
    <div id="cumRap" className=" mt-20 mb-20">
      <div
        className="container-tab mt-10 "
        style={{ backgroundColor: "white" }}
      >
        <Tabs
          style={{ height: 600 }}
          tabPosition="left"
          defaultActiveKey="1"
          items={renderHeThongRap()}
        />
      </div>
    </div>
  );
}
