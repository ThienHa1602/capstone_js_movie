import moment from "moment/moment";
import React from "react";

export default function ChildrenTabMovie({ listMovie }) {
  return (
    <div style={{ height: 600 }} className="overflow-y-auto p-5">
      {listMovie.map((phim) => {
        return (
          <div className="flex mb-5 gap-5">
            <img className="w-24 h-48 " src={phim.hinhAnh} alt="" />
            <div>
              <h1 className="text-2xl mb-5 hover:text-orange-500 cursor-pointer">
                {phim.tenPhim}
              </h1>
              <div className="grid grid-cols-5 gap-10">
                {phim.lstLichChieuTheoPhim.slice(0, 10).map((lich) => {
                  return (
                    <div className="gioChieuPhim">
                      <p className="text-black text-lg font-medium">
                        {moment(lich.ngayChieuGioChieu).format("DD-MM-YYYY")}
                      </p>
                      <p className="text-orange-500 font-medium ">
                        {moment(lich.ngayChieuGioChieu).format("LT")}
                      </p>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
}
