import React from "react";

export default function Apps() {
  return (
    <div
      id="ungDung"
      style={{
        backgroundImage:
          "url(http://demo1.cybersoft.edu.vn/static/media/backapp.b46ef3a1.jpg)",
      }}
      className="pt-32 pb-20 bg-contain"
    >
      <div className="container-app">
        <div className=" text-white grid grid-cols-2 gap-8">
          <div className="grid grid-rows-5 gap-4 text-left pt-5 ">
            <p className=" text-3xl font-bold ">Ứng dụng tiện lợi dành cho</p>
            <p className=" text-3xl font-bold">người yêu điện ảnh</p>
            <p className="font-medium">
              Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và
              đổi quà hấp dẫn
            </p>

            <a href="/" className="button_dow">
              APP MIỄN PHÍ - TẢI VỀ NGAY!
            </a>
            <p className=" text-3xl font-bold">
              Có hai phiên bản cho IOS và Android
            </p>
          </div>
          <div className="flex justify-center">
            <img
              style={{ height: 500, borderRadius: 20 }}
              src="http://demo1.cybersoft.edu.vn/static/media/banner-slider-2.454924ec.jpg"
              alt=""
            />
          </div>
        </div>
      </div>
    </div>
  );
}
