import { Carousel } from "antd";
import React, { useEffect, useState } from "react";
import { movieServ } from "../../../services/movieServices";

export default function Banner() {
  const [banner, setBanner] = useState();
  useEffect(() => {
    movieServ
      .getBanner()
      .then((res) => {
        setBanner(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div className="mb-10">
      <Carousel autoplay>
        {banner?.map((item) => {
          return (
            <div>
              <img
                style={{ height: "80vh", width: "100%", display: "block" }}
                src={item.hinhAnh}
                alt=""
              />
            </div>
          );
        })}
      </Carousel>
    </div>
  );
}
