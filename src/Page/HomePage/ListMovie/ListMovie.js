import React, { useEffect, useState } from "react";
import { movieServ } from "../../../services/movieServices";
import CardMovie from "./CardMovie";
import { useDispatch } from "react-redux";
import {
  batLoadingAction,
  tatLoadingAction,
} from "../../../redux/action/spinnerAction";

export default function ListMovie() {
  const [movies, setMovies] = useState();
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(batLoadingAction());
    movieServ
      .getMovieList()
      .then((res) => {
        dispatch(tatLoadingAction());
        console.log(res.data.content);
        setMovies(res.data.content);
      })
      .catch((err) => {
        dispatch(tatLoadingAction());
        console.log(err);
      });
  }, []);
  return (
    <div
      id="lichChieu"
      className=" grid grid-cols-1 sm:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-10 container mx-auto"
    >
      {movies?.slice(0, 20).map((item) => {
        return <CardMovie movie={item} />;
      })}
    </div>
  );
}
