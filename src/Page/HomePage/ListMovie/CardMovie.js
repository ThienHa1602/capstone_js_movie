import { Card } from "antd";
import React from "react";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function CardMovie({ movie }) {
  let { tenPhim, hinhAnh } = movie;
  return (
    <NavLink to={`/detail/${movie.maPhim}`}>
      <Card
        hoverable
        style={{
          width: "100%",
        }}
        cover={
          <img
            style={{ height: 200, objectFit: "cover", objectPosition: "top" }}
            alt="example"
            src={hinhAnh}
          />
        }
      >
        <Meta title={tenPhim} />
        <button className=" mt-5 hover:text-orange-500">Xem chi tiết</button>
      </Card>
    </NavLink>
  );
}
