import { Button, Form, Input, message } from "antd";
import React from "react";
import bg_animate from "../../assets/login_animate.json";
import Lottie from "lottie-react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { localUserServ } from "../../services/localServices";
import { setUserActionThunk } from "../../redux/action/userAction";

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinishThunk = (values) => {
    let handleSuccess = (res) => {
      message.success("Đăng nhập thành công");
      localUserServ.set(res.data.content);
      navigate("/");
    };
    dispatch(setUserActionThunk(values, handleSuccess));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="w-screen h-screen flex items-center justify-center">
      <div className="rounded p-10 bg-white w-11/12 flex items-center">
        <div className="w-1/2 h-full">
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 16,
            }}
            style={{
              maxWidth: 600,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinishThunk}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <Button type="primary" htmlType="submit" danger>
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
        <div className="w-1/2 h-full flex items-center justify-center">
          <Lottie
            style={{ width: 200 }}
            animationData={bg_animate}
            loop={true}
          />
        </div>
      </div>
    </div>
  );
}
