import axios from "axios";
import { BASE_URL, configHeaders } from "./config";

export const movieServ = {
  getBanner: () => {
    return axios.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachBanner`, {
      headers: configHeaders(),
    });
  },
  getMovieList: () => {
    return axios.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07`, {
      headers: configHeaders(),
    });
  },
  getMovieByTheater: () => {
    return axios.get(
      `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP03`,
      { headers: configHeaders() }
    );
  },
  getDetailMovie: (id) => {
    return axios.get(
      `${BASE_URL}/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,
      { headers: configHeaders() }
    );
  },
  getMovieById: (id) => {
    return axios.get(
      `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`,
      { headers: configHeaders() }
    );
  },
};
